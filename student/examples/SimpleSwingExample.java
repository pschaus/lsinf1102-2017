package student.examples;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


 
public class SimpleSwingExample {
 
	public static void main(String[] args) {

		JFrame frame = new JFrame("Frame Title");
	

		JButton btn = new JButton("click");
		frame.getContentPane().add(btn);

		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				System.out.println("clicked!");
			}
		});

		int width = 300;
		int height = 300;
		frame.setSize(width, height);

		frame.setVisible(true);
	}
}
